<?php
include('inc/db_connection.php');
include('inc/loadInitialData.php');

$id = $_GET['id'];
$queryLoadArticle = "SELECT * FROM articulos WHERE id=".$id;
$doQueryLoadArticle = mysql_query($queryLoadArticle);
$articleData = array();

while($row = mysql_fetch_array($doQueryLoadArticle)) {
	array_push($articleData, $row['id']);
	array_push($articleData, $row['name']);
	array_push($articleData, $row['category']);
	array_push($articleData, $row['cover_img']);
	array_push($articleData, $row['content']);
	array_push($articleData, $row['art_related_1']);
	array_push($articleData, $row['art_related_2']);
	array_push($articleData, $row['date']);
	array_push($articleData, $row['gallery']);
	array_push($articleData, $row['audio']);
	array_push($articleData, $row['youtube']);
}
$categoryInt = (int)$articleData[2];
$related1Int = (int)$articleData[5];
$related2Int = (int)$articleData[6];
?>
<form action="<?php echo 'articleEdit.php?id='.$id; ?>" method="POST" enctype="multipart/form-data">
	<div class="col-xs-12">
		<div class="form-group">
			<label for="articleNameEdit">Nombre del Artículo</label>
			<input type="text" class="form-control" id="articleNameEdit" name="articleNameEdit" placeholder="Nombre del Artículo" value="<?php echo $articleData[1]; ?>">
		</div>
	</div>
	<div class="col-xs-12">
		<div class="form-group">
			<label for="articleCategoryEdit">Categoría</label>
			<select name="articleCategoryEdit" id="articleCategoryEdit" class="form-control">
				<option value="0">Selecciona una categoría</option>
				<?php

				for ($i = 1; $i < $categoriasCount; $i++) {
					if ($i === $categoryInt) {
						echo '<option value="'.$i.'" selected>'.$categories[$i].'</option>';
					} else {
						echo '<option value="'.$i.'">'.$categories[$i].'</option>';
					}
				}

				?>
			</select>
		</div>
		<div class="form-group">
			<label for="articleCoverImageEdit">Imagen de portada (JPG - 1100 x 500 px)</label>
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<?php echo '<img src="images/articles_cover/'.$articleData[3].'" alt="" class="img-responsive" style="margin-bottom:15px;">' ?>
				</div>
			</div>
			<input type="file" class="form-control" id="articleCoverImageEdit" name="articleCoverImageEdit">
		</div>
	</div>
	<div class="col-xs-12">
		<div class="form-group">
			<label for="articleContentEdit">Contenido del Artículo</label>
			<textarea name="articleContentEdit" id="articleContentEdit" rows="20"><?php echo $articleData[4]; ?></textarea>
		</div>
		<div class="form-group">
			<label for="articleRelatedEdit1">Artículo Relacionado 1</label>
			<select name="articleRelatedEdit1" id="articleRelatedEdit1" class="form-control">
				<option value="0">Selecciona un artículo</option>
				<?php

				
				for ($i = 0; $i < $idCount; $i++) {
					if ($articulosIds[$i] === $articleData[5]) {
						if (strlen($articulosTitulos[$i]) > 75) {
							echo '<option value="'.$articulosIds[$i].'" selected>'.substr($articulosTitulos[$i],0,75).'...</option>';
						} else {
							echo '<option value="'.$articulosIds[$i].'" selected>'.$articulosTitulos[$i].'</option>';
						}
					} else {
						if (strlen($articulosTitulos[$i]) > 75) {
							echo '<option value="'.$articulosIds[$i].'">'.substr($articulosTitulos[$i],0,75).'...</option>';
						} else {
							echo '<option value="'.$articulosIds[$i].'">'.$articulosTitulos[$i].'</option>';
						}
					}
					
				}

				?>
			</select>
		</div>
		<div class="form-group">
			<label for="articleRelatedEdit2">Artículo Relacionado 2</label>
			<select name="articleRelatedEdit2" id="articleRelatedEdit2" class="form-control">
				<option value="0">Selecciona un artículo</option>
				<?php

				for ($i = 0; $i < $idCount; $i++) {
					if ($articulosIds[$i] === $articleData[6]) {
						if (strlen($articulosTitulos[$i]) > 75) {
							echo '<option value="'.$articulosIds[$i].'" selected>'.substr($articulosTitulos[$i],0,75).'...</option>';
						} else {
							echo '<option value="'.$articulosIds[$i].'" selected>'.$articulosTitulos[$i].'</option>';
						}
					} else {
						if (strlen($articulosTitulos[$i]) > 75) {
							echo '<option value="'.$articulosIds[$i].'">'.substr($articulosTitulos[$i],0,75).'...</option>';
						} else {
							echo '<option value="'.$articulosIds[$i].'">'.$articulosTitulos[$i].'</option>';
						}
					}
					
				}

				?>
			</select>
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary" value="Guardar cambios">
		</div>
	</div>
</form>
<script>
        CKEDITOR.replace('articleContentEdit',{
        	resize_enabled : false,
        	height : 300
        });
    </script>