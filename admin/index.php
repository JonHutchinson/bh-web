<?php
include('inc/db_connection.php');
include('inc/loadInitialData.php');

// echo $_SESSION['sessionStarted'].' / '.$_SESSION['databaseSelected'];
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Baltazar Hinojosa Ochoa</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
	<link rel="stylesheet" href="../css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/bhadmin-css1.css" />
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">BH ADMIN</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="#">Artículos <span class="sr-only">(current)</span></a></li>
					<li><a href="#">Cerrar Sesión</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- Modal Editar Articulo -->
	<div class="modal fade" id="modalEditArticle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Editar Artículo</h4>
				</div>
				<div class="modal-body">
					<div class="row" id="editArticleModalWrapper"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Agregar Articulo -->
	<div class="modal fade" id="modalAddArticle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Agregar Artículo</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<form action="articleAdd.php" method="POST" enctype="multipart/form-data">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="articleName">Nombre del Artículo</label>
									<input type="text" class="form-control" name="articleName" id="articleName" placeholder="Nombre del Artículo">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group">
									<label for="articleCategory">Categoría</label>
									<select name="articleCategory" id="articleCategory" class="form-control">
										<option value="0">Selecciona una categoría</option>
										<?php

										for ($i = 1; $i < $categoriasCount; $i++) {
											echo '<option value="'.$i.'">'.$categories[$i].'</option>';
										}

										?>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group">
									<label for="articleCoverImage">Imagen de portada (JPG 1100 x 500 px)</label>
									<input type="file" class="form-control" name="articleCoverImage" id="articleCoverImage">
								</div>
							</div>
							<div class="col-xs-12">
								<div class="form-group">
									<label for="articleContent">Contenido del Artículo</label>
									<textarea name="articleContent" id="articleContent" rows="20"></textarea>
								</div>
								<div class="form-group">
									<label for="articleRelated1">Artículo Relacionado 1</label>
									<select name="articleRelated1" id="articleRelated1" class="form-control">
										<option value="0">Selecciona un artículo</option>
										<?php

										for ($i = 0; $i < $idCount; $i++) {
											if (strlen($articulosTitulos[$i]) > 75) {
												echo '<option value="'.$articulosIds[$i].'">'.substr($articulosTitulos[$i],0,75).'...</option>';
											} else {
												echo '<option value="'.$articulosIds[$i].'">'.$articulosTitulos[$i].'</option>';
											}
										}

										?>
									</select>
								</div>
								<div class="form-group">
									<label for="articleRelated2">Artículo Relacionado 2</label>
									<select name="articleRelated2" id="articleRelated2" class="form-control">
										<option value="0">Selecciona un artículo</option>
										<?php

										for ($i = 0; $i < $idCount; $i++) {
											if (strlen($articulosTitulos[$i]) > 75) {
												echo '<option value="'.$articulosIds[$i].'">'.substr($articulosTitulos[$i],0,75).'...</option>';
											} else {
												echo '<option value="'.$articulosIds[$i].'">'.$articulosTitulos[$i].'</option>';
											}
										}

										?>
									</select>
								</div>
								<div class="form-group">
									<input type="submit" class="btn btn-primary" value="Guardar artículo">
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Agregar Categorías -->
	<div class="modal fade" id="modalCategories" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Administrar Categorías</h4>
				</div>
				<div class="modal-body">
					<div id="alertDeleteCatWrapper"></div>
					<h3>Lista de categorías</h3>
					<table class="table table-hover" style="margin-bottom:50px;">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<?php

							for ($i = 1; $i < $categoriasCount; $i++) {
								echo '<tr>
										<form action="categoryEdit.php?id='.$categoriesIds[$i].'" method="POST" enctype="multipart/form-data">
										<td><input type="text" class="form-control" id="category'.$categoriesIds[$i].'" name="category'.$categoriesIds[$i].'" value="'.$categories[$i].'"></td>
										<td width="150">
											<input type="submit" class="btn btn-primary btn-sm" value="Renombrar">
											<a href="javascript:deleteCategoryConfirm('.$categoriesIds[$i].',\''.$categories[$i].'\');" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span></a>
										</td>
										</form>
									</tr>';
							}

							?>
						</tbody>
					</table>
					<h3>Agregar categoría</h3>
					<form action="categoryAdd.php" method="POST">
						<div class="form-group">
							<!-- <label for="categoryName">Nombre de la Categoría</label> -->
							<input type="text" class="form-control" id="categoryName" name="categoryName" placeholder="Nombre de la Categoría">
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Guardar nueva categoría">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<!-- <button type="button" class="btn btn-primary">Guardar Artículo</button> -->
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Multimedia Imagen -->
	<div class="modal fade" id="modalImages" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-picture"></span> Agregar Imágenes</h4>
				</div>
				<div class="modal-body" id="modalImagesWrapper"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<!-- <button type="button" class="btn btn-primary">Guardar Artículo</button> -->
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Multimedia Video -->
	<div class="modal fade" id="modalVideos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-film"></span> Agregar Videos</h4>
				</div>
				<div class="modal-body">
					<h3>Lista de links de videos (YouTube)</h3>
					<table class="table table-hover" style="margin-bottom:50px;">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><a href="https://www.youtube.com/watch?v=CXio7MEBcBg" target="_blank">https://www.youtube.com/watch?v=CXio7MEBcBg</a></td>
								<td width="30">
									<a href="" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
								</td>
							</tr>
							<tr>
								<td><a href="https://www.youtube.com/watch?v=CXio7MEBcBg" target="_blank">https://www.youtube.com/watch?v=CXio7MEBcBg</a></td>
								<td width="30">
									<a href="" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
								</td>
							</tr>
							<tr>
								<td><a href="https://www.youtube.com/watch?v=CXio7MEBcBg" target="_blank">https://www.youtube.com/watch?v=CXio7MEBcBg</a></td>
								<td width="30">
									<a href="" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
								</td>
							</tr>
						</tbody>
					</table>
					<h3>Agregar video (YouTube)</h3>
					<form action="articleAddVideoConfirm.php" method="POST">
						<div class="form-group">
							<!-- <label for="categoryName">Nombre de la Categoría</label> -->
							<input type="text" class="form-control" id="newYoutubeVideo" placeholder="Link de YouTube">
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Guardar nuevo video">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<!-- <button type="button" class="btn btn-primary">Guardar Artículo</button> -->
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Multimedia Audio -->
	<div class="modal fade" id="modalAudios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-headphones"></span> Agregar Audios</h4>
				</div>
				<div class="modal-body">
					<h3>Lista de audios</h3>
					<table class="table table-hover" style="margin-bottom:50px;">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<audio controls>
										<source src="audio/bh-articulo-9.mp3" type="audio/mp3">
										Your browser does not support the audio element.
									</audio>
								</td>
								<td width="30">
									<a href="" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
								</td>
							</tr>
							<tr>
								<td>
									<audio controls>
										<source src="audio/bh-articulo-9.mp3" type="audio/mp3">
										Your browser does not support the audio element.
									</audio>
								</td>
								<td width="30">
									<a href="" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
								</td>
							</tr>
							<tr>
								<td>
									<audio controls>
										<source src="audio/bh-articulo-9.mp3" type="audio/mp3">
										Your browser does not support the audio element.
									</audio>
								</td>
								<td width="30">
									<a href="" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
								</td>
							</tr>
						</tbody>
					</table>
					<h3>Agregar audio (MP3)</h3>
					<form action="articleAddAudioConfirm.php" method="POST">
						<div class="form-group">
							<!-- <label for="categoryName">Nombre de la Categoría</label> -->
							<input type="file" class="form-control" id="newAudio">
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Guardar nuevo audio">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<!-- <button type="button" class="btn btn-primary">Guardar Artículo</button> -->
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div id="alertDeleteWrapper"></div>
			<?php

			if(isset($_GET['error'])) {
				echo '<div class="alert alert-danger alert-dismissible" role="alert" id="alertError">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  '.$errores[$_GET['error']].'
					  </div>';
			}

			if(isset($_GET['success'])) {
				echo '<div class="alert alert-success alert-dismissible" role="alert" id="alertSuccess">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  '.$success[$_GET['success']].'
					  </div>';
			}

			?>
		</div>
		<div class="row">
			<h1>Listado de artículos</h1>
			<a class="btn btn-success" href="#" role="button" data-toggle="modal" data-target="#modalAddArticle"><span class="glyphicon glyphicon-plus"></span> Agregar Artículo</a>
			<a class="btn btn-primary" href="#" role="button" data-toggle="modal" data-target="#modalCategories"><span class="glyphicon glyphicon-th-list"></span> Administrar Categorías</a>
		</div>
		<div class="row">
			<table class="table table-hover article-list">
				<thead>
					<tr>
						<th>Cover</th>
						<th>Nombre</th>
						<th>Fecha</th>
						<th>Categoría</th>
						<th>Agregar Multimedia</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<?php

					for ($i = 0; $i < $articlesCount; $i++) {
						echo '<tr>
								<td class="td-image"><img src="images/articles_cover/'.$articulosCoverImg[$i].'" width="100" border="0" /></td>
								<td class="td-nombre"><a href="javascript:editArticleModal('.$articulosIds[$i].');">'.$articulosTitulos[$i].'</a></td>
								<td class="td-fecha">'.$articulosDates[$i].'</td>
								<td class="td-cat">'.$categories[$articulosCategorias[$i]].'</td>
								<td class="td-multimedia">
									<a href="javascript:editImagesModal('.$articulosIds[$i].');" class="btn btn-default"><span class="glyphicon glyphicon-picture"></span> Imagen</a>
									<a href="" class="btn btn-default" data-toggle="modal" data-target="#modalVideos"><span class="glyphicon glyphicon-film"></span> Video</a>
									<a href="" class="btn btn-default" data-toggle="modal" data-target="#modalAudios"><span class="glyphicon glyphicon-headphones"></span> Audio</a>
								</td>
								<td>
									<a href="javascript:editArticleModal('.$articulosIds[$i].');" class="btn btn-primary" ><span class="glyphicon glyphicon-pencil"></span></a>
									<a href="javascript:deleteArticleConfirm('.$articulosIds[$i].',\''.$articulosTitulos[$i].'\');" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
								</td>
							</tr>';
					}

					?>
				</tbody>
			</table>
		</div>
	</div>
	<script src="../js/jquery-2.1.1.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/bh-js.js"></script>
	<script src="ckeditor/ckeditor.js"></script>
	<script src="inc/bhAdminJS.js"></script>
	
	<script>
        CKEDITOR.replace('articleContent',{
        	resize_enabled : false,
        	height : 300
        });
        CKEDITOR.replace('articleContentEdit',{
        	resize_enabled : false,
        	height : 300
        });
    </script>
</body>
</html>