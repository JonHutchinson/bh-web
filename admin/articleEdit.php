<?php
include('inc/db_connection.php');

$id = $_GET['id'];
$queryLoadArticles = "SELECT * FROM articulos WHERE id=".$id;
$doQueryLoadArticles = mysql_query($queryLoadArticles);
$oldArticleData = array();
$newArticleData = array();

while($row = mysql_fetch_array($doQueryLoadArticles)) {
	array_push($oldArticleData, $row['id']);
	array_push($oldArticleData, $row['name']);
	array_push($oldArticleData, $row['category']);
	array_push($oldArticleData, $row['cover_img']);
	array_push($oldArticleData, $row['content']);
	array_push($oldArticleData, $row['art_related_1']);
	array_push($oldArticleData, $row['art_related_2']);
	array_push($oldArticleData, $row['date']);
	array_push($oldArticleData, $row['gallery']);
	array_push($oldArticleData, $row['audio']);
	array_push($oldArticleData, $row['youtube']);
}


$newArticleData[0] = $id;
$newArticleData[1] = $_POST['articleNameEdit'];
$newArticleData[2] = $_POST['articleCategoryEdit'];
$newArticleData[3] = $oldArticleData[3];
$newArticleData[4] = $_POST['articleContentEdit'];
$newArticleData[5] = $_POST['articleRelatedEdit1'];
$newArticleData[6] = $_POST['articleRelatedEdit2'];

$queriesArray = array(
	'',
	'UPDATE articulos SET name = "',
	'UPDATE articulos SET category =',
	'',
	'UPDATE articulos SET content ="',
	'UPDATE articulos SET art_related_1 =',
	'UPDATE articulos SET art_related_2 ='
);

$articleNewImageType = $_FILES['articleCoverImageEdit']['type'];
$articleNewImageTmp = $_FILES['articleCoverImageEdit']['tmp_name'];
$articleNewImageError = $_FILES['articleCoverImageEdit']['error'];

$diferencesAll = 0;
$diferencesText = 0;
$diferencesImage = 0;
$itemsChanged = array(0,0,0,0,0,0,0);

for ($i = 0; $i <= 6; $i++) {
	if ($newArticleData[$i] != $oldArticleData[$i]) {
		$diferencesText++;
		$diferencesAll++;
		$itemsChanged[$i] = 1;
	}
}
if ($articleNewImageError === 0) {
	$diferencesImage++;
	$diferencesAll++;
}

if ($diferencesAll > 0) {
	if ($diferencesText > 0 && $diferencesImage === 0) {
		// Cambio de texto
		for ($i = 0; $i <= 6; $i++) {
			if ($itemsChanged[$i] === 1) {
				if ($i === 1) {
					$queryEditArticle = $queriesArray[$i].$newArticleData[$i].'" WHERE id='.$id;
				} else if ($i === 4) {
					$queryEditArticle = $queriesArray[$i].mysql_real_escape_string($newArticleData[$i]).'" WHERE id='.$id;
				} else {
					$queryEditArticle = $queriesArray[$i].$newArticleData[$i].' WHERE id='.$id;
				}
				
				$doQueryEditArticle = mysql_query($queryEditArticle);

				if (mysql_affected_rows() >= 1) {
					header('Location: index.php?success=2');
				} else {
					header('Location: index.php?error=6');
				}
			}
		}
	} else if ($diferencesText > 0 && $diferencesImage > 0) {
		// Cambio de texto e imagen
		if (move_uploaded_file($articleNewImageTmp, 'images/articles_cover/'.$oldArticleData[3])) {
			for ($i = 0; $i <= 6; $i++) {
				if ($itemsChanged[$i] === 1) {
					if ($i === 1) {
						$queryEditArticle = $queriesArray[$i].$newArticleData[$i].'" WHERE id='.$id;
					} else if ($i === 4) {
						$queryEditArticle = $queriesArray[$i].mysql_real_escape_string($newArticleData[$i]).'" WHERE id='.$id;
					} else {
						$queryEditArticle = $queriesArray[$i].$newArticleData[$i].' WHERE id='.$id;
					}
					
					$doQueryEditArticle = mysql_query($queryEditArticle);

					if (mysql_affected_rows() >= 1) {
						header('Location: index.php?success=2');
					} else {
						header('Location: index.php?error=6');
					}
				}
			}
		} else {
			header('Location: index.php?error=3');
		}
	} else if ($diferencesText === 0 && $diferencesImage > 0) {
		// Cambio de imagen
		if (mysql_affected_rows() >= 1) {
			if (move_uploaded_file($articleNewImageTmp, 'images/articles_cover/'.$oldArticleData[3])) {
				header('Location: index.php?success=2');
			} else {
				header('Location: index.php?error=3');
			}
		} else {
			header('Location: index.php?error=6');
		}
	}
} else {
	header('Location: index.php?error=5');
}

?>