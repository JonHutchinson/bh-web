<?php
include('inc/db_connection.php');
include('inc/loadInitialData.php');

$id = $_GET['id'];
$queryLoadSelectedArticle = "SELECT * FROM articulos WHERE id=".$id;
$doQueryLoadSelectedArticle = mysql_query($queryLoadSelectedArticle);
$articleSelectedInfo = array();
$allImages = array();
$allImagesArray = array();
$allImagesExplode = '';

while($row = mysql_fetch_array($doQueryLoadSelectedArticle)) {
	array_push($articleSelectedInfo, $row['id']);
	array_push($articleSelectedInfo, $row['name']);
	array_push($articleSelectedInfo, $row['category']);
	array_push($articleSelectedInfo, $row['cover_img']);
	array_push($articleSelectedInfo, $row['content']);
	array_push($articleSelectedInfo, $row['art_related_1']);
	array_push($articleSelectedInfo, $row['art_related_2']);
	array_push($articleSelectedInfo, $row['date']);
	array_push($articleSelectedInfo, $row['gallery']);
	array_push($articleSelectedInfo, $row['audio']);
	array_push($articleSelectedInfo, $row['youtube']);
}

if ($articleSelectedInfo[8] != 0) {
	$queryLoadOldImagesArray = "SELECT * FROM imagenes WHERE article=".$id;
	$doQueryLoadOldImagesArray = mysql_query($queryLoadOldImagesArray);

	while($row = mysql_fetch_array($doQueryLoadOldImagesArray)) {
		array_push($allImages, $row['images']);
	}

	$allImagesArray = explode(',', $allImages);
}

$newImageOrigName = $_FILES['articleNewImages']['name'];
$newImageTmp = $_FILES['articleNewImages']['tmp_name'];
$newImageError = $_FILES['articleNewImages']['error'];

$imgSuccess = array();
$imgError = array();
$imgErrorText = array();
$numberOfImages = count($_FILES['articleNewImages']['name']);

if (!empty($_FILES['articleNewImages'])) {
	foreach ($_FILES['articleNewImages']['name'] as $key => $name) {
		$newImageName = md5_file($_FILES['articleNewImages']['tmp_name'][$key]).'.jpg';
		if ($key != ($numberOfImages - 1)) {
			echo $key.' de '.($numberOfImages - 1).'<br />';
			if ($_FILES['articleNewImages']['type'][$key] === 'image/jpeg') {
				if (move_uploaded_file($_FILES['articleNewImages']['tmp_name'][$key], 'images/articles_images/'.$newImageName)) {
					$imgSuccess[] = $_FILES['articleNewImages']['name'][$key];
					$allImagesArray[] = $newImageName;
				} else {
					$imgError[] = $_FILES['articleNewImages']['name'][$key];
					$imgErrorText[] = 'Error de conexión.';
				}
			} else {
				$imgError[] = $_FILES['articleNewImages']['name'][$key];
				$imgErrorText[] = 'El formato de la imagen debe ser JPG.';
			}
		} else {
			echo $key.' de '.($numberOfImages - 1).' - TERMINA<br />';
			if ($_FILES['articleNewImages']['type'][$key] === 'image/jpeg') {
				if (move_uploaded_file($_FILES['articleNewImages']['tmp_name'][$key], 'images/articles_images/'.$newImageName)) {
					echo 'Se guardaron las imágenes.<br />';
					$imgSuccess[] = $_FILES['articleNewImages']['name'][$key];
					$allImagesArray[] = $newImageName;
					$allImagesExplode = implode(',', $allImagesArray);
					if ($articleSelectedInfo[8] != 0) {
						$querySaveNewImage = "UPDATE imagenes SET images='".$allImagesExplode."' WHERE article=".$id;
					} else {
						$querySaveNewImage = "INSERT INTO `imagenes`(`id`, `article`, `images`) VALUES (0,".$id.",'".$allImagesExplode."')";
					}
					echo $querySaveNewImage.'<br />';
					$doQuerySaveNewImage = mysql_query($querySaveNewImage);
				} else {
					$imgError[] = $_FILES['articleNewImages']['name'][$key];
					$imgErrorText[] = 'Error de conexión.';
				}
			} else {
				$imgError[] = $_FILES['articleNewImages']['name'][$key];
				$imgErrorText[] = 'El formato de la imagen debe ser JPG.';
			}
		}
	}
}

// if (count($newImageOrigName) > 0) {
// 	foreach ($newImageOrigName as $img => $imageName) {
// 		echo $imageName.'<br />';
// 	}
// } else {
// 	header('Location: index.php?error=11');
// }


// $queryLoadOldImagesArray = "SELECT * FROM articulos WHERE id=".$id;
// $doQueryLoadOldImagesArray = mysql_query($queryLoadOldImagesArray);
// $imagesSaved = mysql_num_rows($doQueryLoadOldImagesArray);
// $oldImagesArray = array();

// if ($imagesSaved > 0) {
// 	while($row = mysql_fetch_array($doQueryLoadOldImagesArray)) {
// 		// array_push($articleData, $row['id']);
// 	}
// } else {
// 	$querySaveNewImage = "INSERT INTO `imagenes`(`id`, `article`, `images`) VALUES (0,".$id.",'".."'";
// 	$doQuerySaveNewImage = mysql_query($querySaveNewImage);

// 	if (mysql_affected_rows() >= 1) {
// 		header('Location: index.php?success=0');
// 	}
// }


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Baltazar Hinojosa Ochoa</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
	<link rel="stylesheet" href="../css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/bhadmin-css1.css" />
</head>
<body class="gallery-upload-body">
	<div class="container">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 gallery-upload-confirm-wrapper">
				<h1>Imágenes guardadas</h1>
				<h5>Para artículo: <b><?php echo $articleSelectedInfo[1]; ?></b></h5>
				<?php

				for ($i = 0; $i < count($imgSuccess); $i++) {
					echo '<p><span class="glyphicon glyphicon-ok ico-success"></span> '.$imgSuccess[$i].'</p>';
				}
				for ($i = 0; $i < count($imgError); $i++) {
					echo '<p class="img-error"><span class="glyphicon glyphicon-remove ico-error"></span> '.$imgError[$i].' <span class="error-text">'.$imgErrorText[$i].'</span></p>';
				}

				?>
			</div>
		</div>
	</div>
	<script src="../js/jquery-2.1.1.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/bh-js.js"></script>
</body>
</html>