<?php

$queryLoadCategories = "SELECT * FROM categorias";
$doQueryLoadCategories = mysql_query($queryLoadCategories);

$queryLoadAllArticles = "SELECT * FROM articulos order by id desc";
$doQueryLoadAllArticles = mysql_query($queryLoadAllArticles);

// if (mysql_num_rows($doQueryLoadAllArticles)>0) {
// 	$row = mysql_fetch_array($doQueryLoadAllArticles);
// }
$articulosIds = array();
$categoriesIds = array(0);
$categories = array('Todas');
$articulosTitulos = array();
$articulosCategorias = array();
$articulosCoverImg = array();
$articulosContents = array();
$articulosRelated1 = array();
$articulosRelated2 = array();
$articulosDates = array();
$articulosGallery = array();
$articulosAudio = array();
$articulosYoutube = array();

$errores = array(
	'No hay error.',
	'Debes llenar todos los datos para guardar un nuevo <b>artículo</b>.',
	'El formato de las imágenes del <b>artículo</b> debe ser JPG.',
	'Ha ocurrido un error de conexión al guardar las imágenes del <b>artículo</b>. Por favor inténtalo nuevamente.',
	'Ha ocurrido un error de conexión al tratar de eliminar el <b>artículo</b>. Por favor inténtalo nuevamente.',
	'No se detectaron cambios en el <b>artículo</b>.',
	'Ha ocurrido un error de conexión al tratar de editar el <b>artículo</b>. Por favor inténtalo nuevamente.',
	'Debes llenar todos los datos para guardar una <b>categoría</b> nueva.',
	'Ha ocurrido un error de conexión al tratar de guardar la <b>categoría</b> nueva. Por favor inténtalo nuevamente.',
	'Ha ocurrido un error de conexión al tratar de eliminar la <b>categoría</b>. Por favor inténtalo nuevamente.',
	'Ha ocurrido un error de conexión al tratar de editar la <b>categoría</b>. Por favor inténtalo nuevamente.',
	'No se ha detectado ninguna archivo de <b>imagen</b> para guardar.'
);

$success = array(
	'El <b>artículo</b> ha sido guardado exitosamente.',
	'El <b>artículo</b> ha sido eliminado exitosamente.',
	'El <b>artículo</b> se ha modificado exitosamente.',
	'La nueva <b>categoría</b> se ha guardado exitosamente.',
	'La nueva <b>categoría</b> ha sido eliminada exitosamente.',
	'La <b>categoría</b> se ha modificado exitosamente.'
);

while($row = mysql_fetch_array($doQueryLoadCategories)) {
	array_push($categories, $row['categoria']);
	array_push($categoriesIds, $row['id']);
}

while($row = mysql_fetch_array($doQueryLoadAllArticles)) {
	array_push($articulosIds, $row['id']);
	array_push($articulosTitulos, $row['name']);
	array_push($articulosCategorias, $row['category']);
	array_push($articulosCoverImg, $row['cover_img']);
	array_push($articulosContents, $row['content']);
	array_push($articulosRelated1, $row['related_1']);
	array_push($articulosRelated2, $row['related_2']);
	array_push($articulosDates, $row['date']);
	array_push($articulosGallery, $row['gallery']);
	array_push($articulosAudio, $row['audio']);
	array_push($articulosYoutube, $row['youtube']);
}

$articlesCount = count($articulosTitulos);
$categoriasCount = count($categories);
$highestId = max($articulosIds);
$idCount = count($articulosIds);

?>