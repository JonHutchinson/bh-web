var closeAlertTimer = function(){
  $('#alertError').alert('close');
  $('#alertSuccess').alert('close');
};
setTimeout(closeAlertTimer, 4000);

function deleteArticleConfirm(id,name) {
	$('#alertDeleteArticle').alert('close');
	$('html, body').animate({
		scrollTop: $('body').offset().top
	},700);
	$('#alertDeleteWrapper').append('<div class="alert alert-warning alert-dismissible" id="alertDeleteArticle" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>¿Deseas eliminar el artículo <strong>' + name + '</strong>?<div style="margin-top:20px;"><a href="articleDelete.php?id=' + id + '" class="btn btn-warning" style="margin-right:10px;">Eliminar</a><button type="button" class="btn btn-default" data-dismiss="alert" aria-label="Close">Cancelar</button></div></div>');
}
function deleteCategoryConfirm(id,name) {
	$('#alertDeleteCat').alert('close');
	$('html, body').animate({
		scrollTop: $('body').offset().top
	},700);
	$('#alertDeleteCatWrapper').append('<div class="alert alert-warning alert-dismissible" id="alertDeleteCat" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>¿Deseas eliminar la categoría <strong>' + name + '</strong>?<div style="margin-top:20px;"><a href="categoryDelete.php?id=' + id + '" class="btn btn-warning" style="margin-right:10px;">Eliminar</a><button type="button" class="btn btn-default" data-dismiss="alert" aria-label="Close">Cancelar</button></div></div>');
}
function editArticleModal(id) {
	$('#modalEditArticle').modal('show');
	$( "#editArticleModalWrapper" ).load( "articleEditForm.php?id=" + id, function( response, status, xhr ) {
		if ( status == "error" ) {
			var msg = "Error al cargar el contenido: ";
			$( this ).html( '<div class="col-xs-12">' + msg + xhr.status + " " + xhr.statusText + '</div>' );
		}
	});
}
function editImagesModal(id) {
	$('#modalImages').modal('show');
	$( "#modalImagesWrapper" ).load( "articleEditImagesForm.php?id=" + id, function( response, status, xhr ) {
		if ( status == "error" ) {
			var msg = "Error al cargar el contenido: ";
			$( this ).html( '<div class="col-xs-12">' + msg + xhr.status + " " + xhr.statusText + '</div>' );
		}
	});
}
