<?php
include('inc/db_connection.php');

$articleName = $_POST['articleName'];
$articleCategory = $_POST['articleCategory'];
$articleContent = mysql_escape_string($_POST['articleContent']);
$articleRelated1 = $_POST['articleRelated1'];
$articleRelated2 = $_POST['articleRelated2'];

$articleImageName = mt_rand(1000, 9999).'-'.mt_rand(1000, 9999).'-'.mt_rand(1000, 9999).'.jpg';
$articleImageType = $_FILES['articleCoverImage']['type'];
$articleImageTmp = $_FILES['articleCoverImage']['tmp_name'];
$articleImageError = $_FILES['articleCoverImage']['error'];

if ($articleName != '') {
	if($articleCategory != 0) {
		if ($articleContent != '') {
			if ($articleImageError === 0) {
				if ($articleImageType === 'image/jpeg') {
					saveNewArticle($articleName,$articleCategory,$articleContent,$articleRelated1,$articleRelated2,$articleImageName,$articleImageTmp);
				} else {
					header('Location: index.php?error=2');
				}
			} else {
				header('Location: index.php?error=1');
			}
		} else {
			header('Location: index.php?error=1');
		}
	} else {
		header('Location: index.php?error=1');
	}
} else {
	header('Location: index.php?error=1');
} 

// echo ('Nombre: '.$articleName);
// echo ('Categoria: '.$articleCategory);
// echo ('Contenido: '.$articleContent);
// echo ('Related 1: '.$articleRelated1);
// echo ('Related 2: '.$articleRelated2);
// echo ('Imagen: '.$articleImageName);

function saveNewArticle($nombre,$category,$content,$related1,$related2,$coverImage,$coverImageTmp) {
	if (move_uploaded_file($coverImageTmp, 'images/articles_cover/'.$coverImage)) {
		$querySaveNewArticle = "INSERT INTO `articulos`(`id`, `name`, `category`, `cover_img`, `content`, `art_related_1`, `art_related_2`, `date`, `gallery`, `audio`, `youtube`) VALUES (0,'".$nombre."',".$category.",'".$coverImage."','".$content."',".$related1.",".$related2.",NOW(),0,0,0)";
		$doQuerySaveNewArticle = mysql_query($querySaveNewArticle);

		if (mysql_affected_rows() >= 1) {
			header('Location: index.php?success=0');
		}
	} else {
		header('Location: index.php?error=3');
	}
}

?>