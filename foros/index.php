
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Baltazar Hinojosa Ochoa - Foros</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
	<meta property="og:title" content="Baltazar Hinojosa Ochoa">
	<meta itemprop="name" content="Baltazar Hinojosa Ochoa">
	<meta name="description" content="Creo en el trabajo arduo para la construcción de acuerdos, en las propuestas incluyentes. Creo en la lucha pacífica diaria, creo en la legalidad, las instituciones y la democracia. Creo en México y su gente. Por eso trabajo para lograr un México con igualdad de oportunidades. Por un México justo y seguro en donde se premie el mérito y la dedicación. Por un México plural y democrático en donde cada voz valga lo mismo. Creo en un México en donde se hagan las cosas bien.">
	<meta property="og:description" content="Creo en el trabajo arduo para la construcción de acuerdos, en las propuestas incluyentes. Creo en la lucha pacífica diaria, creo en la legalidad, las instituciones y la democracia. Creo en México y su gente. Por eso trabajo para lograr un México con igualdad de oportunidades. Por un México justo y seguro en donde se premie el mérito y la dedicación. Por un México plural y democrático en donde cada voz valga lo mismo. Creo en un México en donde se hagan las cosas bien.">
	<meta itemprop="description" content="Creo en el trabajo arduo para la construcción de acuerdos, en las propuestas incluyentes. Creo en la lucha pacífica diaria, creo en la legalidad, las instituciones y la democracia. Creo en México y su gente. Por eso trabajo para lograr un México con igualdad de oportunidades. Por un México justo y seguro en donde se premie el mérito y la dedicación. Por un México plural y democrático en donde cada voz valga lo mismo. Creo en un México en donde se hagan las cosas bien.">
	<meta property="og:type" content="website" />
	<meta name="keywords" content="Baltazar, Hinojosa, Ochoa, Matamoros, Reynosa, Ciudad, Victoria, Tampico, Mante, Nuevo, Laredo, México, Diputado, Presidente, Tamaulipas, Gobierno">
	<meta property="og:url" content="http://baltazarhinojosa.com ><http://baltazarhinojosa.com/>" />
	<link rel="canonical" href="http://baltazarhinojosa.com ><http://baltazarhinojosa.com/>" />
	<meta name="robots" content="index, follow" />
	<meta property="og:site_name" content="Baltazar Hinojosa Ochoa" />
	<link rel="shortcut icon" type="image/x-icon" href="http://baltazarhinojosa.com/ ><http://baltazarhinojosa.com/sitio/../images/favicon.ico>sitio/ <http://baltazarhinojosa.com/sitio/images/favicon.ico>images/favicon.ico <http://baltazarhinojosa.com/sitio/images/favicon.ico>">
	<link rel="image_src" href="http://baltazarhinojosa.com/sitio/images/imgfb.jpg ><http://baltazarhinojosa.com/sitio/images/imgfb.jpg>" />
	<meta property="og:image" content="http://baltazarhinojosa.com/ ><http://baltazarhinojosa.com/sitio/images/imgfb.jpg>sitio/ <http://baltazarhinojosa.com/sitio/images/imgfb.jpg>images/imgfb.jpg <http://baltazarhinojosa.com/sitio/images/imgfb.jpg>" />
	<meta itemprop="image" content="http://baltazarhinojosa.com/ ><http://baltazarhinojosa.com/sitio/images/imgfb.jpg>sitio/ <http://baltazarhinojosa.com/sitio/images/imgfb.jpg>images/imgfb.jpg <http://baltazarhinojosa.com/sitio/images/imgfb.jpg>">
	<meta property="al:android:url" content="http://baltazarhinojosa.com ><http://baltazarhinojosa.com/>" />
	<meta property="al:android:package" content="mx.hlcn.bhh" />
	<meta property="al:android:app_name" content="Baltazar Hinojosa" />
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
	<link rel="stylesheet" href="../css/bootstrap.min.css" />
	<link rel="stylesheet" href="../css/bhweb-css1.css" />
</head>
<body>
	<section class="container-fluid header-descarga-app">
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html">
						<img src="../images/main/logo-bh-1.jpg" class="logo-normal visible-md visible-lg" border="0" title="Baltazar Hinojosa" />
						<img src="../images/main/logo-bh-1.jpg" class="logo-small visible-sm" border="0" title="Baltazar Hinojosa" />
						<img src="../images/main/logo-bh-1.jpg" class="logo-xsmall visible-xs" border="0" title="Baltazar Hinojosa" />
					</a>
				</div>
				
				<!-- Menu Normal -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="../index.html"><span class="glyphicon glyphicon-home"></span></span></a></li>
						<li><a href="../baltazar.html">Conoce a Baltazar</a></li>
						<li><a href="../experiencia.html">Mi experiencia</a></li>
						<li><a href="../vision.html">Visión y compromiso</a></li>
						<li><a href="../agenda.html">Agenda legislativa</a></li>
						<li><a href="../informacion.html">Comunicación</a></li>
						<li><a href="../involucrate.html">Participemos Juntos</a></li>
						<!-- <li><a href="gentebuena/index.html">Gente Buena</a></li> -->
						<!-- <li><a href="contacto.html"><span class="glyphicon glyphicon-envelope"></span></a></li> -->
					</ul>
				</div>
			</div>
		</nav>
	</section>
	<div class="container-fluid">
		<section class="titulo-wrapper">
			<div class="container">
				<div class="col-sm-8">
					<h3>Foros</h3>
				</div>
				<div class="ico-rs-interiores col-sm-4 hidden-xs">
					<a href="https://es.wikipedia.org/wiki/Baltazar_Hinojosa_Ochoa" target="_blank">
						<img src="../images/main/banner-ico-wikipedia-2.png" alt="Wikipedia">
					</a>
					<a href="https://instagram.com/baltazarxtam" target="_blank">
						<img src="../images/main/banner-ico-instagram-2.png" alt="Instagram">
					</a>
					<a href="https://www.youtube.com/user/baltazarhinojosa" target="_blank">
						<img src="../images/main/banner-ico-youtube-2.png" alt="YouTube">
					</a>
					<a href="https://twitter.com/BaltazarxTam" target="_blank">
						<img src="../images/main/banner-ico-twitter-2.png" alt="Twitter">
					</a>
					<a href="https://www.facebook.com/BaltazarMHO" target="_blank">
						<img src="../images/main/banner-ico-facebook-2.png" alt="Facebook">
					</a>
				</div>
			</div>
		</section>
		<div class="banner-rs-xs visible-xs">
			<div class="container">
				<div class="row">
					<a href="https://www.facebook.com/BaltazarMHO" target="_blank" title="Facebook">
						<img src="../images/main/ico-rs-xs-facebook.jpg" border="0" />
					</a>
					<a href="https://twitter.com/BaltazarxTam" target="_blank" title="Twitter">
						<img src="../images/main/ico-rs-xs-twitter.jpg" border="0" />
					</a>
					<a href="https://www.youtube.com/user/baltazarhinojosa" target="_blank" title="YouTube">
						<img src="../images/main/ico-rs-xs-youtube.jpg" border="0" />
					</a>
					<a href="https://instagram.com/baltazarxtam" target="_blank" title="Instagram">
						<img src="../images/main/ico-rs-xs-instagram.jpg" border="0" />
					</a>
					<a href="https://es.wikipedia.org/wiki/Baltazar_Hinojosa_Ochoa" target="_blank" title="Wikipedia">
						<img src="../images/main/ico-rs-xs-wikipedia.jpg" border="0" />
					</a>
				</div>
			</div>
		</div>
		<section class="forum-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="txt-rojo">Nombre de la Categoría</h3>
					</div>
				</div>
				<div class="row">
					<ul class="cats">
						<?php

						$queryLoadProposals = "SELECT * FROM forums order by date asc";
						$doQueryLoadProposals = mysql_query($queryLoadProposals);
						$numProposals = mysql_num_rows($doQueryLoadProposals);

						while($row = mysql_fetch_array($doQueryLoadProposals)) {
							$forumListId = $row['id'];
							$forumListName = $row['forum'];
							$forumListCategory = $row['category'];
							$forumListDate = $row['date'];
							$forumListDescription = $row['description'];
							$forumListImage = $row['image'];
							
							echo '<li>';
								echo '<article class="row cat">';
									echo '<div class="col-xs-12">';
										// echo '<img src="../images/demo.jpg" alt="" class="img-responsive">';
									echo '<div style="width:100%; height:200px; background:url(forumAdmin/images/'.$forumListImage.') no-repeat; background-size:cover; background-position:center;"></div>';
									echo '</div>';
									echo '<div class="col-xs-12">';
										echo '<div class="prop-tit">';
											echo '<span><i class="glyphicon glyphicon-calendar"></i>'.$forumListDate.'</span>';
											echo '<h4>'.$forumListName.'</h4>';
										echo '</div>';
										echo '<div class="prensa-article-desc">';
											echo '<p>'.$forumListDescription.'</p>';
										echo '</div>';
											echo '<a href="articulos/articulo12.html" class="ask" data-toggle="modal" data-target="#myModal">Proponer / Preguntar</a>';
									echo '</div>';
								echo '</article>';
							echo '</li>';
						}

						?>
						<ul class="cats">
						
							<li>
								<article class="row cat">
									<div class="col-xs-12">
									<div style="width:100%; height:200px; background:url('../images/demo.jpg') no-repeat; background-size:cover; background-position:center;"></div>
									</div>
									<div class="col-xs-12">
										<div class="prop-tit">
											<span><i class="glyphicon glyphicon-calendar"></i>20/12/2016</span>
											<h4>Este es el nombre del tituloEste es el nombre del tituloEste es el nombre del tituloEste es el nombre del titulo</h4>
										</div>
										<div class="prensa-article-desc">
											<p> Sed fringilla neque eu justo consequat, a tincidunt quam dignissim. Ut imperdiet at sem nec porta. Aliquam ac odio sem. Nullam sagittis gravida libero rutrum maximus. Aenean et neque nec eros volutpat mollis.</p>
										</div>
											<a href="articulos/articulo12.html" class="ask" data-toggle="modal" data-target="#myModal">Proponer / Preguntar</a>
									</div>
								</article>
						<!-- <li>
							<article class="row cat">
								<div class="col-xs-12">
									<img src="../images/demo.jpg" alt="" class="img-responsive">
								</div>
								<div class="col-xs-12">
									<div class="prop-tit">
										<span><i class="glyphicon glyphicon-calendar"></i><span>21 Oct 2015</span>
										<h4><a href="articulos/articulo12.html">Duis id finibus dolor, id vehicula ante. Praesent sed tellus vel diam dapibus dapibus.</a></h4>
										
									</div>
									<div class=" prensa-article-desc">
										Sed fringilla ipsum sit amet libero iaculis molestie. Duis id finibus dolor, id vehicula ante. Praesent sed tellus vel diam dapibus dapibus. Morbi molestie sem ut sapien porttitor porttitor. id vehicula ante. Praesent sed tellus vel diam dapibus dapibus. Morbi molestie sem ut sapien porttitor porttitor. id vehicula ante. Praesent sed tellus vel diam dapibus dapibus. Morbi molestie sem ut sapien porttitor porttitor. 
									</div>
										<a href="articulos/articulo12.html" class="ask" data-toggle="modal" data-target="#myModal">Proponer / Preguntar</a>
								</div>
							</article>
						</li>
						<li>
							<article class="row cat">
								<div class="col-xs-12">
									<img src="../images/demo.jpg" alt="" class="img-responsive">
								</div>
								<div class="col-xs-12">
									<div class="prop-tit">
										<span><i class="glyphicon glyphicon-calendar"></i><span>21 Oct 2015</span>
										<h4><a href="articulos/articulo12.html">Duis id finibus dolor, id vehicula ante. Praesent sed tellus vel diam dapibus dapibus.</a></h4>
										
									</div>
									<div class=" prensa-article-desc">
										Sed fringilla ipsum sit amet libero iaculis molestie. Duis id finibus dolor, id vehicula ante. Praesent sed tellus vel diam dapibus dapibus. Morbi molestie sem ut sapien porttitor porttitor. 
									</div>
										<a href="articulos/articulo12.html" class="ask" data-toggle="modal" data-target="#myModal">Proponer / Preguntar</a>
								</div>
							</article>
						</li>
						<li>
							<article class="row cat">
								<div class="col-xs-12">
									<img src="../images/demo.jpg" alt="" class="img-responsive">
								</div>
								<div class="col-xs-12">
									<div class="prop-tit">
										<span><i class="glyphicon glyphicon-calendar"></i><span>21 Oct 2015</span>
										<h4><a href="articulos/articulo12.html">Duis id finibus dolor, id vehicula ante. Praesent sed tellus vel diam dapibus dapibus.</a></h4>
										
									</div>
									<div class=" prensa-article-desc">
										Sed fringilla ipsum sit amet libero iaculis molestie. Duis id finibus dolor, id vehicula ante. Praesent sed tellus vel diam dapibus dapibus. Morbi molestie sem ut sapien porttitor porttitor. 
									</div>
										<a href="articulos/articulo12.html" class="ask" data-toggle="modal" data-target="#myModal">Proponer / Preguntar</a>
								</div>
							</article>
						</li>
						<li>
							<article class="row cat">
								<div class="col-xs-12">
									<img src="../images/demo.jpg" alt="" class="img-responsive">
								</div>
								<div class="col-xs-12">
									<div class="prop-tit">
										<span><i class="glyphicon glyphicon-calendar"></i><span>21 Oct 2015</span>
										<h4><a href="articulos/articulo12.html">Duis id finibus dolor, id vehicula ante. Praesent sed tellus vel diam dapibus dapibus.</a></h4>
										
									</div>
									<div class=" prensa-article-desc">
										Sed fringilla ipsum sit amet libero iaculis molestie. Duis id finibus dolor, id vehicula ante. Praesent sed tellus vel diam dapibus dapibus. Morbi molestie sem ut sapien porttitor porttitor. 
									</div>
										<a href="articulos/articulo12.html" class="ask" data-toggle="modal" data-target="#myModal">Proponer / Preguntar</a>
								</div>
							</article>
						</li> -->
					</ul><!--Fin categoria-->
				</div>
			</div>
		</section>

		<footer class="footer-wrapper">
			<div class="footer-container container">
				<div class="row">
					<div class="pull-left">
						<img src="../images/main/logo-bh-2.jpg" border="0" style="margin-right:20px;" />
					</div>
					<div class="logos-rs pull-right">
						<a href="https://www.facebook.com/BaltazarMHO" target="_blank" title="Facebook"><img src="../images/main/ico-facebook.jpg" border="0" /></a>
						<a href="https://twitter.com/BaltazarxTam" target="_blank" title="Twitter"><img src="../images/main/ico-twitter-2.jpg" border="0" /></a>
						<a href="https://www.youtube.com/user/baltazarhinojosa" target="_blank" title="YouTube"><img src="../images/main/ico-youtube.jpg" border="0" /></a>
						<a href="https://instagram.com/baltazarxtam" target="_blank" title="Instagram"><img src="../images/main/ico-instagram.jpg" border="0" /></a>
						<a href="https://es.wikipedia.org/wiki/Baltazar_Hinojosa_Ochoa" target="_blank" title="Wikipedia"><img src="../images/main/ico-wikipedia.jpg" border="0" /></a>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog" role="document">
	    	<div class="modal-content">
		      	<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h3 class="modal-title" id="myModalLabel">Propuesta / Pregunta</h3>
		      	</div>
	      		<div class="modal-body">
			      	<div id="login-form">
				      	<div class="row cspace">
						  <div class="col-lg-10 col-lg-offset-1">
						      <input type="mail" id="login-mail" class="form-control" placeholder="Tu Email">
						  </div><!-- /.col-lg-6 -->
						</div>
						<div class="row cspace">
						  <div class="col-lg-10 col-lg-offset-1">
						  	<button type="button" class="btn btn-default can" data-dismiss="modal">Cancelar</button>
			        		<button type="button" class="btn btn-primary go">Ingresar</button>
						  </div>
						</div><!-- /.row -->
						<div class="row cspace">
						  <div class="col-lg-10 col-lg-offset-1 txt-cnt">
						  	<h5>¿Aun no estas registrado?</h5>
						  	<a class="forum-register" href="#">Haz clic aquí</a>
						  </div>
						</div><!-- /.row -->
					</div>
					<div id="registro-form">
						<h3>Completa el registro</h3>
						<hr>
						<div class="row cspace">
						  <div class="col-lg-12">
						      <input type="text" class="form-control" placeholder="Nombre Completo">
						  </div><!-- /.col-lg-6 -->
						</div>
						<div class="row cspace">
						  <div class="col-lg-6">
						      <input type="text" class="form-control" placeholder="Ciudad">
						  </div><!-- /.col-lg-6 -->
						  <div class="col-lg-6">
						      <input type="text" class="form-control" placeholder="Colonia">
						  </div><!-- /.col-lg-6 -->
						</div>
						<div class="row cspace">
						  <div class="col-lg-6">
						      <input type="tel" class="form-control" placeholder="Teléfono">
						  </div><!-- /.col-lg-6 -->
						  <div class="col-lg-6">
						      <input type="mail" id="register-mail" class="form-control" placeholder="Email">
						  </div><!-- /.col-lg-6 -->
						</div>
						<div class="row cspace">
						  <div class="col-lg-6">
						  	<button type="button" class="btn btn-default can" data-dismiss="modal">Cancelar</button>
			        		<button type="button" class="btn btn-primary go">Ingresar</button>
						  </div>
						</div><!-- /.row -->
					</div>
					<div id="propuesta-form">
						<h3>Escribe tu propuesta / pregunta</h3>
						<hr>
						<div class="row cspace">
						  <div class="col-lg-12">
						      <textarea class="form-control" placeholder="Tu mensaje debe contener 400 caracteres máximo"></textarea>
						  </div><!-- /.col-lg-6 -->
						</div>
						<div class="row cspace">
						  <div class="col-lg-6 col-lg-offset-6">
						  	<button type="button" class="btn btn-default can" data-dismiss="modal">Cancelar</button>
			        		<button type="button" class="btn btn-primary go">Ingresar</button>
						  </div>
						</div><!-- /.row -->
					</div>
	      		</div>
	    	</div>
	  	</div>
	</div>
	<script src="../js/jquery-2.1.1.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/bh-js.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-68165293-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<script type="text/javascript">
    $(document).ready(function(){
        $("#login-mail").keyup(function(){
            $('#register-mail').val($('#login-mail').val());
        });
    })

</script>
</body>
</html>