<?php
include("res/db_conn.php");
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Baltazar Hinojosa Ochoa - Foros</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
	<link rel="stylesheet" href="../../css/bootstrap.min.css" />
	<link rel="stylesheet" href="../../css/bhweb-css1.css" />
</head>
<body>
	<section class="container-fluid header-descarga-app">
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html">
						<img src="../../images/main/logo-bh-1.jpg" class="logo-normal visible-md visible-lg" border="0" title="Baltazar Hinojosa" />
						<img src="../../images/main/logo-bh-1.jpg" class="logo-small visible-sm" border="0" title="Baltazar Hinojosa" />
						<img src="../../images/main/logo-bh-1.jpg" class="logo-xsmall visible-xs" border="0" title="Baltazar Hinojosa" />
					</a>
				</div>
				<!-- Menu Normal -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="../../index.html"><span class="glyphicon glyphicon-home"></span></span></a></li>
						<li><a href="../../baltazar.html">Conoce a Baltazar</a></li>
						<li><a href="../../experiencia.html">Mi experiencia</a></li>
						<li><a href="../../vision.html">Visión y compromiso</a></li>
						<li><a href="../../agenda.html">Agenda legislativa</a></li>
						<li><a href="../../informacion.html">Comunicación</a></li>
						<li><a href="../../involucrate.html">Participemos Juntos</a></li>
						<li><a href="../index.php">Foros</a></li>
						<!-- <li><a href="gentebuena/index.html">Gente Buena</a></li> -->
						<!-- <li><a href="contacto.html"><span class="glyphicon glyphicon-envelope"></span></a></li> -->
					</ul>
				</div>
			</div>
		</nav>
	</section>
	<div class="container-fluid">
		<section class="titulo-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<h3>Foros</h3>
					</div>
				</div>
			</div>
		</section>
		<section class="admin-submenu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<a href="../index.php" class="btn btn-submenu-forums"><span class="glyphicon glyphicon-chevron-left"></span> Regresar a Foros</a>
						<a href="index.php" class="btn btn-submenu-forums">Propuestas</a>
						<a href="forum-users.php" class="btn btn-submenu-forums">Usuarios</a>
						<a href="forum-subjects.php" class="btn btn-submenu-forums btn-forums-active">Foros</a>
						<a href="forum-categories.php" class="btn btn-submenu-forums">Categorías de Foros</a>
					</div>
				</div>
			</div>
		</section>
		<section class="forum-add-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h3>Agregar Foro</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<?php 

						if (isset($_GET['successSub'])) {
							switch ($_GET['successSub']) {
								case 1:
									echo '<div class="alert alert-success alert-dismissible" role="alert">';
										echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
										echo 'El foro se ha guardado correctamente.';
									echo '</div>';
									break;
								case 2:
									echo '<div class="alert alert-success alert-dismissible" role="alert">';
										echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
										echo 'El foro se eliminó correctamente.';
									echo '</div>';
									break;
							}
							
						}

						if (isset($_GET['errorSub'])) {
							switch ($_GET['errorSub']) {
								case 1:
									echo '<div class="alert alert-danger alert-dismissible" role="alert">';
										echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
										echo 'Ha ocurrido un error al guardar el foro. Inténta nuevamente.';
									echo '</div>';
									break;
								case 2:
									echo '<div class="alert alert-danger alert-dismissible" role="alert">';
										echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
										echo 'Ha ocurrido un error al eliminar el foro. Inténta nuevamente.';
									echo '</div>';
									break;
								case 3:
									echo '<div class="alert alert-danger alert-dismissible" role="alert">';
										echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
										echo 'Ha ocurrido un error al guardar la imagen del foro. Inténta nuevamente.';
									echo '</div>';
									break;
								case 4:
									echo '<div class="alert alert-danger alert-dismissible" role="alert">';
										echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
										echo 'No se detectaron cambios en la información del foro.';
									echo '</div>';
									break;
							}
							
						}

						?>
					</div>
				</div>
				<div class="row">
					<?php

					if (!isset($_GET['id'])) {
						echo '<form action="actions/forumAdd.php" method="POST" enctype="multipart/form-data">';
							echo '<div class="col-xs-12 col-sm-6">';
								echo '<div class="form-group">';
									echo '<label for="forumName">Nombre del Foro</label>';
									echo '<input type="text" name="forumName" id="forumName" class="form-control" placeholder="Nombre del Foro">';
								echo '</div>';
							echo '</div>';
							echo '<div class="col-xs-12 col-sm-6">';
								echo '<div class="form-group">';
									echo '<label for="forumCategory">Categoría</label>';
									echo '<select name="forumCategory" id="forumCategory" class="form-control">';
										echo '<option value="0">Selecciona una categoría</option>';

										$queryLoadCategories = "SELECT * FROM categories order by category asc";
										$doQueryLoadCategories = mysql_query($queryLoadCategories);

										while($row = mysql_fetch_array($doQueryLoadCategories)) {
											echo '<option value="'.$row['category'].'">'.$row['category'].'</option>';
										}

									echo '</select>';
								echo '</div>';
							echo '</div>';
							echo '<div class="col-xs-12">';
								echo '<div class="form-group">';
									echo '<label for="forumImage">Imágen (JPG)</label>';
									echo '<input type="file" name="forumImage" id="forumImage" class="form-control" placeholder="Imágen">';
								echo '</div>';
							echo '</div>';
							echo '<div class="col-xs-12">';
								echo '<div class="form-group">';
									echo '<label for="forumDescription">Descripción del foro</label>';
									echo '<p><span id="numCaract">100</span> caracteres</p>';
									echo '<textarea name="forumDescription" id="forumDescription" rows="6" class="form-control" style="resize:none;"></textarea>';
								echo '</div>';
							echo '</div>';
							echo '<div class="col-xs-12 col-sm-6">';
								echo '<div class="form-group">';
									echo '<input type="submit" class="btn btn-success form-control" value="Publicar Foro">';
								echo '</div>';
							echo '</div>';
						echo '</form>';
					} else {
						$queryLoadSelectedForum = "SELECT * FROM forums WHERE id=".$_GET['id'];
						$doQueryLoadSelectedForum = mysql_query($queryLoadSelectedForum);

						while($row = mysql_fetch_array($doQueryLoadSelectedForum)) {
							echo '<form action="actions/forumEdit.php?id='.$_GET['id'].'" method="POST" enctype="multipart/form-data">';
								echo '<div class="col-xs-12 col-sm-6">';
									echo '<div class="form-group">';
										echo '<label for="forumName">Nombre del Foro</label>';
										echo '<input type="text" name="forumName" id="forumName" class="form-control" placeholder="Nombre del Foro" value="'.$row['forum'].'">';
									echo '</div>';
								echo '</div>';
								echo '<div class="col-xs-12 col-sm-6">';
									echo '<div class="form-group">';
										echo '<label for="forumCategory">Categoría</label>';
										echo '<select name="forumCategory" id="forumCategory" class="form-control">';
											echo '<option value="0">Selecciona una categoría</option>';

											$queryLoadCategories = "SELECT * FROM categories order by category asc";
											$doQueryLoadCategories = mysql_query($queryLoadCategories);

											while($categoryList = mysql_fetch_array($doQueryLoadCategories)) {
												if ($categoryList['category'] === $row['category']) {
													echo '<option value="'.$categoryList['category'].'" selected>'.$categoryList['category'].'</option>';
												} else {
													echo '<option value="'.$categoryList['category'].'">'.$categoryList['category'].'</option>';
												}
											}

										echo '</select>';
									echo '</div>';
								echo '</div>';
								echo '<div class="col-xs-12">';
									echo '<div class="form-group">';
										echo '<label for="forumImage">Imágen (JPG)</label>';
										echo '<div>';
											echo '<img src="images/'.$row['image'].'?='.filemtime('images/'.$row['image']).'" border="0" width="200" height="auto" align="left" />';
										echo '</div>';
										echo '<input type="file" name="forumImage" id="forumImage" class="form-control" placeholder="Imágen">';
									echo '</div>';
								echo '</div>';
								echo '<div class="col-xs-12">';
									echo '<div class="form-group">';
										echo '<label for="forumDescription">Descripción del foro</label>';
										echo '<p><span id="numCaract">100</span> caracteres</p>';
										echo '<textarea name="forumDescription" id="forumDescription" rows="6" class="form-control" style="resize:none;">'.$row['description'].'</textarea>';
									echo '</div>';
								echo '</div>';
								echo '<div class="col-xs-12 col-sm-6">';
									echo '<div class="form-group">';
										echo '<input type="submit" class="btn btn-success form-control" value="Guardar Cambios del Foro">';
										echo '<a href="forum-subjects.php" class="btn btn-primary" style="display:block; margin-top:15px;">Cancelar</a>';
									echo '</div>';
								echo '</div>';
							echo '</form>';
						}
					}

					?>
					
				</div>
			</div>
		</section>
		<section class="forum-admin-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="table-responsive" style="margin-bottom:100px;">
							<table class="table">
								<thead>
									<tr>
										<th></th>
										<th>Imagen</th>
										<th>Foro</th>
										<th>Categoría</th>
										<th>Fecha</th>
										<th>Opciones</th>
									</tr>
								</thead>
								<tbody>
									
									<?php

									$queryLoadProposals = "SELECT * FROM forums order by date asc";
									$doQueryLoadProposals = mysql_query($queryLoadProposals);
									$numProposals = mysql_num_rows($doQueryLoadProposals);

									while($row = mysql_fetch_array($doQueryLoadProposals)) {
										$forumListId = $row['id'];
										$forumListName = $row['forum'];
										$forumListCategory = $row['category'];
										$forumListDate = $row['date'];
										$forumListDescription = $row['description'];
										$forumListImage = $row['image'];
										echo '<tr>';
											echo '<td width="5%">'.$i.'</td>';
											echo '<td width="10%"><img src="images/'.$forumListImage.'?='.filemtime('images/'.$forumListImage).'" border="0" width="100%" height="auto" /></td>';
											echo '<td width="40%"><b>'.$forumListName.'</b></td>';
											echo '<td width="10%">'.$forumListCategory.'</td>';
											echo '<td width="10%">'.$forumListDate.'</td>';
											echo '<td width="25%">';
												echo '<a href="forum-subjects.php?id='.$forumListId.'" class="btn btn-primary">Editar</a>';
												echo '<a href="actions/forumDelete.php?id='.$forumListId.'" class="btn btn-danger" style="margin-left:2%;">Eliminar</a>';
											echo '</td>';
										echo '</tr>';
									}

									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<script src="../../js/jquery-2.1.1.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<script src="../../js/bh-js.js"></script>
	<script src="res/bh-forumsJS.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-68165293-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>