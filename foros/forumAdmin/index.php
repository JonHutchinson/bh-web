<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Baltazar Hinojosa Ochoa - Foros</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
	<link rel="stylesheet" href="../../css/bootstrap.min.css" />
	<link rel="stylesheet" href="../../css/bhweb-css1.css" />
</head>
<body>
	<section class="container-fluid header-descarga-app">
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html">
						<img src="../../images/main/logo-bh-1.jpg" class="logo-normal visible-md visible-lg" border="0" title="Baltazar Hinojosa" />
						<img src="../../images/main/logo-bh-1.jpg" class="logo-small visible-sm" border="0" title="Baltazar Hinojosa" />
						<img src="../../images/main/logo-bh-1.jpg" class="logo-xsmall visible-xs" border="0" title="Baltazar Hinojosa" />
					</a>
				</div>
				<!-- Menu Normal -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="../../index.html"><span class="glyphicon glyphicon-home"></span></span></a></li>
						<li><a href="../../baltazar.html">Conoce a Baltazar</a></li>
						<li><a href="../../experiencia.html">Mi experiencia</a></li>
						<li><a href="../../vision.html">Visión y compromiso</a></li>
						<li><a href="../../agenda.html">Agenda legislativa</a></li>
						<li><a href="../../informacion.html">Comunicación</a></li>
						<li><a href="../../involucrate.html">Participemos Juntos</a></li>
						<li><a href="../index.php">Foros</a></li>
						<!-- <li><a href="gentebuena/index.html">Gente Buena</a></li> -->
						<!-- <li><a href="contacto.html"><span class="glyphicon glyphicon-envelope"></span></a></li> -->
					</ul>
				</div>
			</div>
		</nav>
	</section>
	<div class="container-fluid">
		<section class="titulo-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<h3>Foros</h3>
					</div>
				</div>
			</div>
		</section>
		<section class="admin-submenu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<a href="../index.php" class="btn btn-submenu-forums"><span class="glyphicon glyphicon-chevron-left"></span> Regresar a Foros</a>
						<a href="index.php" class="btn btn-submenu-forums btn-forums-active">Propuestas</a>
						<a href="forum-users.php" class="btn btn-submenu-forums">Usuarios</a>
						<a href="forum-subjects.php" class="btn btn-submenu-forums">Foros</a>
						<a href="forum-categories.php" class="btn btn-submenu-forums">Categorías de Foros</a>
					</div>
				</div>
			</div>
		</section>
		<section class="forum-admin-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th></th>
										<th>Autor</th>
										<th>Tipo</th>
										<th>Fecha</th>
										<th>Propuesta</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td width="5%">1</td>
										<td width="20%"><b>Fernando Lozano</b></td>
										<td width="10%">Propuesta</td>
										<td width="10%">25/04/2016</td>
										<td width="45%">
											<p>Bacon ipsum dolor amet bresaola kielbasa picanha leberkas sausage shank, pork chop pork belly tri-tip kevin swine t-bone tongue. Porchetta meatloaf salami cow. Pastrami corned beef beef ribs filet mignon ham hock tongue pork chop t-bone salami shankle cow swine cupim strip steak chuck.</p>
											<p>Bacon frankfurter cupim cow meatball, corned beef hamburger ribeye doner ground round bresaola. Ham drumstick pastrami shank. Filet mignon cow shoulder ground round prosciutto hamburger pork loin beef chicken ham hock landjaeger pork ball tip picanha. Hamburger biltong fatback picanha beef pastrami shank beef ribs sirloin ham brisket pork chop. Porchetta fatback pork loin pork belly, sausage hamburger pork sirloin brisket tongue beef shankle tail.</p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<script src="../../js/jquery-2.1.1.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<script src="../../js/bh-js.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-68165293-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>