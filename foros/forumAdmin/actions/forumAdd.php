<?php

include("../res/db_conn.php");

$forumName = $_POST['forumName'];
$forumCategory = $_POST['forumCategory'];
$forumDescription = $_POST['forumDescription'];

$forumImageName = mt_rand(1000, 9999).'-'.mt_rand(1000, 9999).'-'.mt_rand(1000, 9999).'.jpg';
$forumImageType = $_FILES['forumImage']['type'];
$forumImageTmp = $_FILES['forumImage']['tmp_name'];
$forumImageError = $_FILES['forumImage']['error'];

if ($forumImageType === 'image/jpeg') {
	if (move_uploaded_file($forumImageTmp, '../images/'.$forumImageName)) {
		$querySaveCategory = "INSERT INTO `forums`(`id`, `category`, `forum`, `date`, `description`, `image`) VALUES (0,'".$forumCategory."','".$forumName."',NOW(),'".$forumDescription."','".$forumImageName."')";
		$doQuerySaveCategory = mysql_query($querySaveCategory);

		if (mysql_affected_rows() > 0) {
			header('Location: ../forum-subjects.php?successSub=1');
		} else {
			unlink('../images/'.$forumImageName);
			header('Location: ../forum-subjects.php?errorSub=1');
		}
	} else {
		header('Location: ../forum-subjects.php?errorSub=3');
	}
} else {
	header('Location: ../forum-subjects.php?errorSub=3');
}

?>