var forumDescMaxlength = 150;
$("#forumDescription").attr('maxlength',forumDescMaxlength);
$("#forumDescription").attr('placeholder','Descripción del Foro (máximo ' + forumDescMaxlength + ' caracteres)');
$("#numCaract").text(forumDescMaxlength);
$("#forumDescription").keyup(function(){
  $("#numCaract").text(forumDescMaxlength - $(this).val().length);
});