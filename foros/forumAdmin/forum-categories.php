<?php
include("res/db_conn.php");
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Baltazar Hinojosa Ochoa - Foros</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
	<link rel="stylesheet" href="../../css/bootstrap.min.css" />
	<link rel="stylesheet" href="../../css/bhweb-css1.css" />
</head>
<body>
	<section class="container-fluid header-descarga-app">
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html">
						<img src="../../images/main/logo-bh-1.jpg" class="logo-normal visible-md visible-lg" border="0" title="Baltazar Hinojosa" />
						<img src="../../images/main/logo-bh-1.jpg" class="logo-small visible-sm" border="0" title="Baltazar Hinojosa" />
						<img src="../../images/main/logo-bh-1.jpg" class="logo-xsmall visible-xs" border="0" title="Baltazar Hinojosa" />
					</a>
				</div>
				<!-- Menu Normal -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="../../index.html"><span class="glyphicon glyphicon-home"></span></span></a></li>
						<li><a href="../../baltazar.html">Conoce a Baltazar</a></li>
						<li><a href="../../experiencia.html">Mi experiencia</a></li>
						<li><a href="../../vision.html">Visión y compromiso</a></li>
						<li><a href="../../agenda.html">Agenda legislativa</a></li>
						<li><a href="../../informacion.html">Comunicación</a></li>
						<li><a href="../../involucrate.html">Participemos Juntos</a></li>
						<li><a href="../index.php">Foros</a></li>
						<!-- <li><a href="gentebuena/index.html">Gente Buena</a></li> -->
						<!-- <li><a href="contacto.html"><span class="glyphicon glyphicon-envelope"></span></a></li> -->
					</ul>
				</div>
			</div>
		</nav>
	</section>
	<div class="container-fluid">
		<section class="titulo-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<h3>Foros</h3>
					</div>
				</div>
			</div>
		</section>
		<section class="admin-submenu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<a href="../index.php" class="btn btn-submenu-forums"><span class="glyphicon glyphicon-chevron-left"></span> Regresar a Foros</a>
						<a href="index.php" class="btn btn-submenu-forums">Propuestas</a>
						<a href="forum-users.php" class="btn btn-submenu-forums">Usuarios</a>
						<a href="forum-subjects.php" class="btn btn-submenu-forums">Foros</a>
						<a href="forum-categories.php" class="btn btn-submenu-forums btn-forums-active">Categorías de Foros</a>
					</div>
				</div>
			</div>
		</section>
		<section class="forum-admin-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php 

						if (isset($_GET['successCat'])) {
							switch ($_GET['successCat']) {
								case 1:
									echo '<div class="alert alert-success alert-dismissible" role="alert">';
										echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
										echo 'La categoría se ha guardado correctamente.';
									echo '</div>';
									break;
								case 2:
									echo '<div class="alert alert-success alert-dismissible" role="alert">';
										echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
										echo 'La categoría se eliminó correctamente.';
									echo '</div>';
									break;
							}
							
						}

						if (isset($_GET['errorCat'])) {
							switch ($_GET['errorCat']) {
								case 1:
									echo '<div class="alert alert-warning alert-dismissible" role="alert">';
										echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
										echo 'Ha ocurrido un error al guardar la categoría. Inténta nuevamente.';
									echo '</div>';
									break;
								case 2:
									echo '<div class="alert alert-warning alert-dismissible" role="alert">';
										echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
										echo 'Ha ocurrido un error al eliminar la categoría. Inténta nuevamente.';
									echo '</div>';
									break;
							}
							
						}

						?>
					</div>
					<div class="col-xs-12 col-sm-6">
						<h3>Agregar Categoría</h3>
						<?php

						if (!isset($_GET['id'])) {
							echo '<form action="actions/forumCategoryAdd.php" method="POST" enctype="multipart/form-data">';
								echo '<div class="form-group">';
									echo '<label for="forumCatName">Nombre de la categoría</label>';
									echo '<input type="text" name="forumCatName" id="forumCatName" class="form-control" placeholder="Nombre de la categoría">';
								echo '</div>';
								echo '<div class="form-group">';
									echo '<input type="submit" class="btn btn-success form-control" value="Guardar Categoría">';
								echo '</div>';
							echo '</form>';
						} else {
							$queryLoadSelectedCat = "SELECT * FROM categories WHERE id=".$_GET['id'];
							$doQueryLoadSelectedCat = mysql_query($queryLoadSelectedCat);

							while($row = mysql_fetch_array($doQueryLoadSelectedCat)) {
								echo '<form action="actions/forumCategoryEdit.php?id='.$_GET['id'].'" method="POST" enctype="multipart/form-data">';
									echo '<div class="form-group">';
										echo '<label for="forumCatName">Nombre de la categoría</label>';
										echo '<input type="text" name="forumCatName" id="forumCatName" class="form-control" placeholder="Nombre de la categoría" value="'.$row['category'].'">';
									echo '</div>';
									echo '<div class="form-group">';
										echo '<input type="submit" class="btn btn-success form-control" value="Guardar Cambios">';
										echo '<a href="forum-categories.php" class="btn btn-primary" style="display:block; margin-top:15px;">Cancelar</a>';
									echo '</div>';
								echo '</form>';
							}
						}

						?>
					</div>
					<div class="col-xs-12 col-sm-6">
						<table class="table">
							<thead>
								<tr>
									<th></th>
									<th>Categoría</th>
									<th>Editar</th>
								</tr>
							</thead>
							<tbody>
								<?php

								$queryLoadCategories = "SELECT * FROM categories order by category asc";
								$doQueryLoadCategories = mysql_query($queryLoadCategories);
								$num = 0;

								while($row = mysql_fetch_array($doQueryLoadCategories)) {
									$num++;
									echo '<tr>';
										echo '<td>'.$num.'</td>';
										echo '<td><b>'.$row['category'].'</b></td>';
										echo '<td>';
											echo '<a href="forum-categories.php?id='.$row['id'].'" class="btn btn-primary">Editar</a>';
											echo '<a href="actions/forumCategoryDelete.php?id='.$row['id'].'" class="btn btn-danger" style="margin-left:2%;">Eliminar</a>';
										echo '</td>';
									echo '</tr>';
								}

								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</div>
	<script src="../../js/jquery-2.1.1.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<script src="../../js/bh-js.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-68165293-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>