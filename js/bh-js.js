// console.log('Ha cargado el archivo bh-js.js');
// window.fbAsyncInit = function() {
//   FB.init({
//     appId      : '1058083217552637',
//     xfbml      : true,
//     status     : true,
//     version    : 'v2.4'
//   });
//   console.log('Entró a fbAsyncInit');
//   // getFBInfo();
//   FB.getLoginStatus(function(response) {
//     if (response.status === 'connected') {
//       console.log('conectado a FB');
//       getFBInfo();
//     } else if (response.status === 'not_authorized') {
//       console.log('Sin autorizacion');
//       $('#facebookWall').hide();
//       $('#facebookLogin').show();
//     } else {
//       console.log('NO conectado a FB');
//       $('#facebookWall').hide();
//       $('#facebookLogin').show();
//     }
//   });
// };

// (function(d){
//   var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
//   js = d.createElement('script'); js.id = id; js.async = true;
//   js.src = "//connect.facebook.net/en_US/all.js";
//   d.getElementsByTagName('head')[0].appendChild(js);
// }(document));

// function fbLogin() {
//   FB.login(function(response) {
//    if (response.authResponse) {
//      getFBInfo();
//    } else {
//      console.log('User cancelled login or did not fully authorize.');
//    }
//  });
// }

// function getFBInfo() {
//   console.log('entro a getFBInfo');
//   $('#facebookWall').show();
//   $('#facebookLogin').hide();
//   FB.api(
//     '/BaltazarMHO',
//     'GET',
//     {
//       "fields":"posts.limit(4){id,name,message,full_picture,created_time},events.limit(4){name,place,start_time,id}",
//       "filter":"app_2305272732"
//     },
//     function(response) {
//         console.log(response);
//         for (i = 0; i < 4; i++) {
//           console.log(response.posts.data[i]);

//           var postFoto = response.posts.data[i].full_picture;
//           var postMessage = response.posts.data[i].message;
//           var postName = response.posts.data[i].name;
//           var postLink = 'http://www.facebook.com/' + response.posts.data[i].id;
//           var postDate = response.posts.data[i].created_time;
//           var postDateY = postDate.slice(0,4);
//           var postDateM = postDate.slice(5,7);
//           var postDateD = postDate.slice(8,10);

//           switch (postDateM) {
//             case '01':
//               var postDateM_Strig = 'Enero';
//               break;
//             case '02':
//               var postDateM_Strig = 'Febrero';
//               break;
//             case '03':
//               var postDateM_Strig = 'Marzo';
//               break;
//             case '04':
//               var postDateM_Strig = 'Abril';
//               break;
//             case '05':
//               var postDateM_Strig = 'Mayo';
//               break;
//             case '06':
//               var postDateM_Strig = 'Junio';
//               break;
//             case '07':
//               var postDateM_Strig = 'Julio';
//               break;
//             case '08':
//               var postDateM_Strig = 'Agosto';
//               break;
//             case '09':
//               var postDateM_Strig = 'Septiembre';
//               break;
//             case '10':
//               var postDateM_Strig = 'Octubre';
//               break;
//             case '11':
//               var postDateM_Strig = 'Noviembre';
//               break;
//             case '12':
//               var postDateM_Strig = 'Diciembre';
//               break;
//           }

//           $('#fotoPostFB_' + (i + 1)).css({
//             'background' : 'url(' + postFoto + ')',
//             'background-position' : 'center, center',
//             'background-size' : 'cover'
//           });
//           $('#mensajePostFB_' + (i + 1)).html(postMessage);
//           $('#namePostFB_' + (i + 1)).html(postName);
//           $('#linkPostFB_' + (i + 1)).attr('href',postLink);
//           $('#datePostFB_' + (i + 1)).html(postDateM_Strig + ' ' + postDateY);
//         }
//     }
//   );
// }
//LIGHTBOX------------------------------------------------------------------------------------------------------------------
// $(function() {
//   $('#lightbox img').click(function() {
//     var imageURL = $(this).attr('src');
//     $('.fondo-lightbox').fadeIn(600);
//     $('#imgLightbox').attr('src',imageURL);
//     console.log($('#imgLightbox').naturalWidth);
//     // $('#imgLightbox').css({});
//     $('#imgLightbox').fadeIn(300);
//   })
// });
// PRINCIPAL----------------------------------------------------------------------------------------------------------------
var bannerActive = 1;
var bannerLength = $('.banner-item').children().length;
var bannerTimeInterval = 7000;
var bannerTransition = 1000;

setInterval(function() {
  $('#bannerImg' + bannerActive).fadeOut(bannerTransition);
  $('#txtBanner' + bannerActive).fadeOut(bannerTransition);
  if (bannerActive < bannerLength) {
    bannerActive++;
    $('#bannerImg' + bannerActive).fadeIn(bannerTransition);
    $('#txtBanner' + bannerActive).fadeIn(bannerTransition);
  } else {
    bannerActive = 1;
    $('#bannerImg' + bannerActive).fadeIn(bannerTransition);
    $('#txtBanner' + bannerActive).fadeIn(bannerTransition);
  }
}, bannerTimeInterval);
// TIMELINE-----------------------------------------------------------------------------------------------------------------
var timelineSelected;
var timelineHeight;
var timelineOpen = false;

getTimelineHeight();

$(window).resize(function () {
  getTimelineHeight();
  timelineItemHeight = $('.timeline-description-item').height();
  $('#timelineDescriptionWrapper').css({'height' : timelineItemHeight + 'px'});
});

function getTimelineHeight () {
  timelineHeight = $('#timelineDescriptionWrapper').height() + 60;
  
}

function openTimelineDescription (description) {
  for (i = 1; i <= 6; i++){
    if (description != i) {
      $('#timelineSel' + i).css('display','none');
    } else {
      $('#timelineSel' + i).css('display','block');
    }
  }
  $('#timelineSel' + description).css('display','block');
  if (!timelineOpen) {
    $('#timelineInfo' + description).css('display','block');
    timelineHeight = $('#timelineDescriptionWrapper').height() + 60;

    $('#timelineDescription').animate({
      'height' : timelineHeight + 'px'
    }, function () {
      timelineOpen = true;
    });
  } else {
    $('#timelineDescriptionWrapper').fadeOut(400, function () {
      for (i = 1; i <= 6; i++){
        if (description != i) {
          $('#timelineInfo' + i).css('display','none');
        } else {
          $('#timelineInfo' + i).css('display','block');
        }
      }
      timelineHeight = $('#timelineDescriptionWrapper').height() + 60;
      $('#timelineDescription').animate({
        'height' : timelineHeight + 'px'
      }, function () {
        $('#timelineDescriptionWrapper').fadeIn(400);
      });
    });
  }
  $('html, body').animate({
      scrollTop: $('#micarrera').offset().top
  },700);
}
// VISION----------------------------------------------------------------------------------------------------------------
function ira(url) {
  $('html, body').animate({
      scrollTop: $(url).offset().top
  },700);
}
// GO TO------------------------------------------------------------------------------------------------------------------
var winW = $(window).width();
var scrollPos = $(window).scrollTop();

hideGoToButtonMovile();
hideGoButtonTop();

$(window).resize(function() {
  winW = $(window).width();
  $('#btnGoTo').hide();
  hideGoToButtonMovile();
  hideGoButtonTop();
});
$(window).scroll(function() {
  scrollPos = $(window).scrollTop();
  hideGoButtonTop();
});

function hideGoToButtonMovile() {
  if (winW < 767) {
    $('#btnGoTo').fadeOut(100);
  } else {
    if (scrollPos > 50) {
      $('#btnGoTo').fadeIn(300);
    }
  }
}
function hideGoButtonTop() {
  if (scrollPos <= 50) {
    $('#btnGoTo').fadeOut(100);
  } else {
    if (winW >= 767) {
      $('#btnGoTo').fadeIn(300);
    }
  }
}

function gotop() {
  $('html, body').animate({
      scrollTop: $('body').offset().top
  },700);
}

//BaltazarMHO?fields=posts.limit(4){id,name,full_picture}
//BaltazarMHO?fields=posts.limit(4){id,name,message,full_picture},events.limit(4){name,place,start_time,description}

// Prensa----------------------------------------------------------------------------------------------------------------
$('.prensa-categorias ul li').click(function() {
  window.location.href = $(this).children('a').attr('href');
})

var c = -1;
var m = -1;
var categoriesLength = $('.prensa-categorias ul li a').length;
var articlesLength = $('.prensa-article').length;
var categoriesName = [];
var categorySelected = 0;

$('.prensa-categorias ul li a').each(function() {
  c++;
  categoriesName.push($(this).text());
  $(this).attr('href','javascript:selectCategory(' + c + ');');
});

loadCategory(categorySelected);

for (i = 0; i < categoriesLength; i++) {
  $('.cat' + i + ' .prensa-article-date .badge-category').attr('href','javascript:selectCategory(' + i + ');');
  $('.prensa-container .cat' + i + ' .prensa-article-date .badge-category .cat-name').text(categoriesName[i]);
}
$('.prensa-categorias ul li').each(function() {
  m++;
  $(this).attr('id','selCat' + m);
});

function selectCategory(cat) {
  categorySelected = cat;
  loadCategory(cat);
  $('.prensa-categorias ul li').removeClass('cat-active');
  $('#selCat' + cat).addClass('cat-active');
}

function loadCategory(cat) {
  for (i = 0; i < articlesLength; i++) {
    $('.prensa-article').css('display','none');
    if (cat === 0) {
      $('.prensa-article').fadeIn(300);
    } else {
      $('.cat' + cat).fadeIn(300);
    }
  }
}


// console.log(cantCategories);

// Contacto--------------------------------------------------------------------------------------------------------------
function sendContact() {
  var form_nombre = $('#nombre').val();
  var form_apellidos = $('#apellidos').val();
  var form_email = $('#email').val();
  var form_telefono = $('#telefono').val();
  var form_tema = $('#tema').val();
  var form_comentario = $('#comentario').val();

  jQuery.ajax({
    url: "send_contact.php",
    data:'nombre=' + form_nombre +'&apellidos=' + form_apellidos + '&email=' + form_email + '&telefono=' + form_telefono + '&tema=' + form_tema + '&comentario' + form_comentario,
    type: "POST",
    success:function(data){
        // $("#mail-status").html(data);
        console.log('Enviado')
    },
    error:function (){
      console.log('Error')
    }
  });
}

// Lightbox--------------------------------------------------------------------------------------------------------------

$('.thumb-item').each(function() {
  $(this).css({
    'background-image' : 'url(' + $(this).children().attr('src') + ')',
    'background-size' : 'cover',
    'background-position' : 'center'
  });
});

$('.thumb-item').click(function() {
  var imgSelection = new Image;
  imgSelection.src = $(this).children().attr('src');
  openLightbox(imgSelection.src,imgSelection.width,imgSelection.height);
});

$('.lightbox').click(function() {
  closeLightbox();
});

function openLightbox(img,w,h) {

  if (w >= h) {
    $('.lightbox').append('<img src="' + img + '" border="0" class="lightbox-img lightbox-img-hor" />');
  } else {
    $('.lightbox').append('<img src="' + img + '" border="0" class="lightbox-img lightbox-img-ver" />');
  }

  $('.lightbox').append('<a href="javascript:closeLightbox();" class="lightbox-close-btn"><span class="glyphicon glyphicon-remove"></span></a>');
  
  // $('.lightbox').append('<img src="' + img + '" border="0" id="imgLightbox" />');

  $('.lightbox').fadeIn(500);
}
function closeLightbox() {
  $('.lightbox').fadeOut(300, function() {
    $('.lightbox').html('');
  });
}

// CANVAS

var modalWidth = $('#modalContent').width();

$(window).resize(function() {
  modalWidth = $('#modalContent').width();
  $('#canvas').attr('width', modalWidth + 'px');
  $('#canvas').attr('height', modalWidth + 'px');
});

$('#modalEstoyconBH').on('shown.bs.modal', function (e) {
  modalWidth = $('#modalContent').width();
  $('#canvas').attr('width', modalWidth + 'px');
  $('#canvas').attr('height', modalWidth + 'px');
});

$('#canvas').attr('width', modalWidth + 'px');
$('#canvas').attr('height', modalWidth + 'px');

function createCanvas() {
  console.log(modalWidth);

  var fontSize = modalWidth * 0.1;
  var name = $('#canvasName').val();
  var c = document.getElementById("canvas");
  var ctx = c.getContext("2d");
  var img = document.getElementById("imgCanvas");

  ctx.drawImage(img,0,0,modalWidth,modalWidth);
  ctx.font = "600 " + fontSize + "px Raleway";
  ctx.textAlign="center";
  ctx.fillStyle="#FFFFFF";
  
  ctx.fillText(name,(modalWidth * 0.5),(modalWidth * 0.299295));

  var img = canvas.toDataURL("image/jpg");

  $('#result').html('<p style="color:#999; font-size:12px;">Da click derecho sobre la imágen y selecciona la opción para guardar la imágen en tus archivos.</p><img src="' + img + '" width="800" height="800" class="img-responsive" />');
  $('#result').fadeIn(1000);
}






